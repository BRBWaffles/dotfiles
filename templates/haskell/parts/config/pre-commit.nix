{
  pre-commit.settings.hooks = {
    nixfmt.enable = true;
    commitizen.enable = true;
    statix.enable = true;
  };
}
