let
  configPath = ./config;
  printerPath = configPath + /printers;

  devicesFunctions = {
    ownerWriteOthersReadMask = [
      "fmask=0022"
      "dmask=0022"
    ];
    ownerExclusiveReadWriteMask = [
      "fmask=0077"
      "dmask=0077"
    ];
    readWritePermissions = [
      "rw"
    ];
    sambaPermissions = [
      "rw"
      "gid=100"
      "vers=3.0"
      "x-systemd.automount"
      "x-systemd.requires=network-online.target"
    ];
    fileModeAndDirMode = [
      "file_mode=0644"
      "dir_mode=0755"
    ];
    userIdForUser0 = [
      "uid=1000"
    ];
    userIdForUser1 = [
      "uid=1001"
    ];
    dummy = [
    ];
  };

  deviceswithFunctions = builtins.listToAttrs (
    map
      (name: {
        name = builtins.substring 0 (builtins.stringLength name - 4) name;
        value = import (configPath + "/${name}") {
          inherit
            devicesFunctions
            ;
        };
      })
      (
        builtins.filter (name: builtins.match ".*\\.nix$" name != null) (
          builtins.attrNames (builtins.readDir configPath)
        )
      )
  );

  devicesPrinters = builtins.listToAttrs (
    map
      (name: {
        name = builtins.substring 0 (builtins.stringLength name - 4) name;
        value = import (printerPath + "/${name}") {
          inherit
            devicesFunctions
            ;
        };
      })
      (
        builtins.filter (name: builtins.match ".*\\.nix$" name != null) (
          builtins.attrNames (builtins.readDir printerPath)
        )
      )
  );

  devices = deviceswithFunctions // devicesPrinters;
in
{
  devices = devices;
}
