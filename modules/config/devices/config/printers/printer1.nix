{ devicesFunctions }:
let
  inherit (devicesFunctions)
    dummy
    ;
in
{
  name = dummy;
  label = "Brother-HL-2170W";
  ip = {
    address0 = "192.168.50.195";
  };
}
