{ devicesFunctions }:
let
  inherit (devicesFunctions)
    ownerWriteOthersReadMask
    ;
in
{
  label = "Deimos";
  name = "deimos";
  sync = {
    address0 = "";
  };
  ip = {
    address0 = "192.168.50.142";
  };
  boot = {
    options = ownerWriteOthersReadMask;
  };
}
