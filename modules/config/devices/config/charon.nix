{ devicesFunctions }:
let
  inherit (devicesFunctions)
    ownerExclusiveReadWriteMask
    ;
in
{
  label = "Charon";
  name = "charon";
  sync = {
    address0 = "";
  };
  ip = {
    address0 = "192.168.50.42";
  };
  boot = {
    options = ownerExclusiveReadWriteMask;
  };
}
