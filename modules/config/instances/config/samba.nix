{ instancesFunctions }:
let
  inherit (instancesFunctions)
    jellyfinLabel
    jellyfinName
    servicePath
    sopsPath
    ;

  sambaLabel = "Samba";
  sambaName = "samba";
in
{
  label = sambaLabel;
  name = sambaName;
  sops = {
    path0 = "${sopsPath}/${sambaName}";
  };
  paths = {
    path0 = "${servicePath}/${jellyfinLabel}";
    path1 = jellyfinName;
    path2 = "";
  };
  ports = {
    port0 = 445; # Samba
  };
}
