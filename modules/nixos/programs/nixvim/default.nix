{ pkgs, ... }:
{
  programs.nixvim = {
    enable = true;
    colorschemes.catppuccin = {
      enable = true;
      settings.flavour = "macchiato";
    };
    globals.mapleader = " ";
    keymaps = [
      {
        action = "<cmd>Neotree toggle<CR>";
        key = "<leader>e";
        mode = "n";
        options.silent = true;
      }
    ];

    plugins = {
      avante = {
        enable = true;
        settings = {
          claude = {
            model = "claude-3-5-sonnet-latest";
          };
        };
      };

      trouble.enable = true;
      web-devicons.enable = true;
      conform-nvim = {
        enable = true;
        settings = {
          formatters_by_ft = {

            css = [
              "prettierd"
              "prettier"
            ];
            html = [
              "prettierd"
              "prettier"
            ];
            javascript = [
              "prettierd"
              "prettier"
            ];
            javascriptreact = [ "prettier" ];
            json = [ "prettier" ];
            markdown = [ "prettier" ];
            nix = [ "nixfmt" ];
            rust = [ "rustfmt" ];
            sh = [ "shfmt" ];
            typescript = [
              "prettierd"
              "prettier"
            ];
            typescriptreact = [ "prettier" ];
            yaml = [
              "prettierd"
              "prettier"
            ];
          };
          formatters = {
            asmfmt = {
              command = "asmfmt";
              stdin = true;
            };
          };
        };
        settings.format_on_save = {
          lspFallback = true;
          timeoutMs = 2000;
        };
      };
      direnv = {
        enable = true;
      };
      treesitter = {
        enable = true;
      };
      lean.enable = true;
      nix.enable = true;
      markdown-preview.enable = true;
      zellij.enable = true;
      zellij-nav.enable = true;
      yazi.enable = true;
      gitsigns = {
        enable = true;
        settings = {
          current_line_blame = true;
          trouble = true;
        };
      };
      copilot-chat = {
        enable = true;
      };
      nvim-tree = {
        enable = true;
        openOnSetupFile = true;
        autoReloadOnWrite = true;
      };
      nix-develop.enable = true;
      lazygit.enable = true;
      haskell-scope-highlighting.enable = true;
      lsp = {
        enable = true;
        servers = {
          elmls.enable = true;
          hls = {
            enable = true;
            installGhc = true;
          };
          ghcide.enable = true;
          leanls.enable = true;
          markdown_oxide.enable = true;
          nixd.enable = true;
          nushell.enable = true;
          rls.enable = true;
          rust_analyzer = {
            enable = true;
            installCargo = true;
            installRustc = true;
          };
          yamlls.enable = true;
          java_language_server = {
            package = "java-language-server";
            cmd = [ "java-language-server" ];
          };
        };
      };
    };
    opts = {
      updatetime = 100;
      number = true;
      relativenumber = true;
      splitbelow = true;
      splitright = true;
      scrolloff = 4;

      autoindent = true;
      clipboard = "unnamedplus";
      expandtab = true;
      shiftwidth = 2;
      smartindent = true;
      tabstop = 2;

      ignorecase = true;
      incsearch = true;
      smartcase = true;
      wildmode = "list:longest";

      swapfile = false;
      undofile = true;
    };
    clipboard.providers.wl-copy.enable = true;
    extraPlugins = builtins.attrValues {
      inherit (pkgs)

        ;
    };
  };
}
