{
  nix.settings = {
    substituters = [
      "https://cosmic.cachix.org/"
    ];
    trusted-public-keys = [
      "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE="
    ];
  };

  environment.sessionVariables.COSMIC_DATA_CONTROL_ENABLED = 1;
  services = {
    desktopManager.cosmic.enable = true;
    displayManager.cosmic-greeter.enable = true;
  };
}
