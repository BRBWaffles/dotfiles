{
  config,
  flake,
  lib,
  ...
}:
let
  inherit (flake.config.people)
    user0
    ;
in
{
  services = {
    greetd = {
      enable = true;
      vt = 7;
      settings =
        let
          default_session = {
            command = "${lib.meta.getExe config.programs.hyprland.package}";
            user = user0;
          };
        in
        {
          inherit
            default_session
            ;
          initial_session = default_session;
        };
    };
  };
}
