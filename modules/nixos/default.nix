let
  inherit (import ../helpers.nix) directoryImport;
  modules = directoryImport ./.;
in
{
  flake.nixosModules = {
    mars = {
      imports = builtins.attrValues {
        inherit (modules)
          mullvad
          syncthing
          ollama
          hypr
          wayland
          xserver
          ;
      };
    };

    venus = {
      imports = builtins.attrValues {
        inherit (modules)
          plasma
          sddm
          tablet
          ;
      };
    };

    deimos = {
      imports = builtins.attrValues {
        inherit (modules)
          gnome
          xserver
          ;
      };
    };

    ceres = {
      imports = builtins.attrValues {
        inherit (modules)
          acme
          caddy
          jellyfin
          logrotate
          mastodon
          minecraft
          ollama
          website
          postgresql
          samba
          vaultwarden
          forgejo
          wayland
          xserver
          ;
      };
    };

    mantle = {
      imports = builtins.attrValues {
        inherit (modules)
          sops
          ;
      };
    };

    crust = {
      imports = builtins.attrValues {
        inherit (modules)
          hardware
          programs
          ;
      };
    };

    core = {
      imports = builtins.attrValues {
        inherit (modules)
          core
          ;
      };
    };
  };
}
