{
  flake,
  config,
  ...
}:
let
  inherit (flake.config.people)
    user0
    user1
    user2
    ;
  inherit (flake.config.people.users.${userLogic})
    sshKeys
    ;
  inherit (flake.config.machines)
    devices
    ;
  hostname = config.networking.hostName;
  mars = devices.mars.name;
  ceres = devices.ceres.name;
  venus = devices.venus.name;
  charon = devices.charon.name;

  userLogic =
    if hostname == mars then
      user0
    else if hostname == ceres then
      user0
    else if hostname == venus then
      user2
    else
      "";
in
{
  users.users =
    if hostname == charon then
      { }
    else
      {
        ${userLogic} = {
          openssh.authorizedKeys.keys = sshKeys;
        };
      };
}
