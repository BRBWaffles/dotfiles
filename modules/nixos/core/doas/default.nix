{ flake, config, ... }:
let
  inherit (flake.config.people)
    user0
    user1
    user2
    ;
  inherit (flake.config.machines)
    devices
    ;
  hostname = config.networking.hostName;
  mars = devices.mars.name;
  ceres = devices.ceres.name;
  venus = devices.venus.names;
  charon = devices.charon.name;
in
{
  security = {
    doas = {
      enable = true;
      extraRules = [
        {
          keepEnv = true;
          noPass = true;
          users = [
            (
              if hostname == mars then
                user0
              else if hostname == ceres then
                user0
              else if hostname == charon then
                user1
              else if hostname == venus then
                user2
              else
                ""
            )
          ];
        }
      ];
    };
    # sudo.enable = false;
  };
}
