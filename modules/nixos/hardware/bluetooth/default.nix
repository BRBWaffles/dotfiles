{
  pkgs,
  ...
}:

{
  hardware = {
    bluetooth = {
      enable = true;
      package = pkgs.bluez;
      powerOnBoot = true;
      settings = {
        General = {
          Experimental = true;
          Disable = "Headset";
        };
      };
      disabledPlugins = [
        "sap"
      ];
    };
  };
  services.blueman.enable = true;
}
