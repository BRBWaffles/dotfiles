{ config, ... }:
let
  inherit (import ../helpers.nix) directoryImport;
  modules = directoryImport ./.;
in
{
  flake.homeModules =
    let
      inherit (config.machines.devices)
        charon
        mars
        venus
        deimos
        ceres
        ;
      inherit (config.people) user0 user1 user2;
    in
    {
      "${mars.name}-${user0}" = {
        imports = builtins.attrValues {
          inherit (modules)
            cli
            brave
            floorp
            tor
            ghostty
            prismLauncher
            steam
            zed
            feishin
            obsidian
            libreOffice
            okular
            kolourPaint
            imageViewing
            obsStudio
            daVinci
            mpv
            freetube
            discordCanary
            element
            signal
            sharing
            bitwarden
            emote
            flameshot
            gnomeCalculator
            scrcpy
            systemMonitor
            usbImager
            virtManager
            hypr
            wayland
            theming
            polychromatic
            ;
        };
      };
      "${mars.name}-${user1}" = {
        imports = builtins.attrValues {
          inherit (modules)
            cli
            brave
            ghostty
            zed
            gaming
            spotify
            docs
            mpv
            kolourPaint
            discord
            signal
            scrcpy
            bitwarden
            emote
            desktop
            ;
        };
      };
      "${venus.name}-${user2}" = {
        imports = builtins.attrValues {
          inherit (modules)
            cli
            brave
            firefoxNix
            braveNix
            code
            ghostty
            gaming
            spotify
            audioProduction
            wpsOffice
            obsidian
            okular
            images
            modeling
            obsStudio
            kdenlive
            videoPlaying
            discord
            signal
            tdesktop
            teams
            whatsApp
            zoom
            tools
            ;
        };
      };
      "${deimos.name}-${user0}" = {
        imports = builtins.attrValues {
          inherit (modules)
            cli
            brave
            emulators
            firefox
            tor
            code
            gaming
            media
            messaging
            sharing
            tools
            ;
        };
      };
      "${deimos.name}-${user1}" = {
        imports = builtins.attrValues {
          inherit (modules)
            cli
            brave
            ghostty
            zed
            gaming
            spotify
            docs
            mpv
            kolourPaint
            discord
            signal
            scrcpy
            bitwarden
            emote
            desktop
            ;
        };
      };
      "${charon.name}-${user1}" = {
        imports = builtins.attrValues {
          inherit (modules)
            cli
            catppuccin
            gtk
            brave
            ghostty
            zed
            gaming
            spotify
            docs
            mpv
            jellyfin
            kolourPaint
            discord
            signal
            scrcpy
            bitwarden
            emote
            desktop
            ;
        };
      };
      "${ceres.name}-${user0}" = {
        imports = builtins.attrValues {
          inherit (modules)
            cli
            tooling
            ;
        };
      };
    };
}
