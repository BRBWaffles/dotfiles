{
  embed-thumbnail = true;
  embed-subs = true;
  sub-langs = "english";
  downloader = "aria2c";
  downloader-args = "aria2c:'-c -x8 -s8 -k1M'";
}
