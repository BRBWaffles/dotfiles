{
  "Super Space" = "rm -r /home/$USER/.cache/tofi* ; tofi-drun";
  "Super C" = "zed";
  "Super A" = "obsidian";
  "Super D" = "ghostty -e zellij a dotfiles";
  "Super E" = "ghostty -e nu -e y";
  "Super T" = "ghostty";
  "Super G" = "firefox https://cronometer.com/#diary";
  "Super B" = "firefox";
  "Super V" = "vesktop";
  "Super W" = "bitwarden";
  "Super H" = "feishin";
  "Super Y" = "scrcpy";
  "Super R" = "ghostty -e nu -e btm";
  "Super N" = "signal-desktop";
  "Super M" = "element-desktop";
  "Super Home" = "sudo protonvpn c --cc CA";
  "Super End" = "exit";
  "Super S" = "steam";
  "Super period" = "emote";
}
