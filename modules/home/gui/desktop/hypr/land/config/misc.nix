{
  focus_on_activate = 1;
  force_default_wallpaper = 0;
  disable_hyprland_logo = true;
  disable_splash_rendering = true;
  disable_hyprland_qtutils_check = true;
}
