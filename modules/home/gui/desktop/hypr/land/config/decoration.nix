{
  active_opacity = 0.95;
  inactive_opacity = 0.90;
  fullscreen_opacity = 1;
  rounding = 10;
  dim_inactive = 1;
  dim_strength = 0.05;

  shadow = {
    enabled = true;
    color = "rgba(00000000)";
    ignore_window = true;
    offset = "0 2";
    range = 20;
    render_power = 3;
    scale = 0.97;
  };

  blur = {
    enabled = true;

    size = 5;
    passes = 2;

    brightness = 1;
    contrast = 1.300000;
    ignore_opacity = true;
    noise = 0.011700;

    new_optimizations = true;

    xray = true;
    popups = true;
    popups_ignorealpha = 0.2;
  };
}
