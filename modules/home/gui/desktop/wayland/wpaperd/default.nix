{
  programs.wpaperd = {
    enable = true;
    settings = {
      "default" = {
        path = "~/Files/Projects/dotfiles/modules/home/gui/desktop/wayland/wpaperd/wallpaper";
        apply-shadow = true;
        duration = "1m";
        sorting = "random";
      };
    };
  };
}
