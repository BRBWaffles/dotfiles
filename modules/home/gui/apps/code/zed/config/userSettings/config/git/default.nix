{
  enabled = true;
  autoFetch = true;
  autoFetchInterval = 300;
  git_status = true;
  git_gutter = "tracked_files";
}
