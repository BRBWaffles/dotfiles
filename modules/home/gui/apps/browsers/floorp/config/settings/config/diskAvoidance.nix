{
  # DISK AVOIDANCE
  "browser.cache.disk.enable" = false;
  "browser.privatebrowsing.forceMediaMemoryCache" = true;
  "media.memory_cache_max_size" = 65536;
  "browser.sessionstore.privacy_level" = 2;
  "toolkit.winRegisterApplicationRestart" = false;
  "browser.shell.shortcutFavicons" = false;
}
