{
  # OPTIONAL HARDENING
  # These settings are commented out in the original template; uncomment if needed
  # "mathml.disabled" = true;
  # "svg.disabled" = true;
  # "gfx.font_rendering.graphite.enabled" = false;
  # "javascript.options.asmjs" = false;
  # "javascript.options.ion" = false;
  # "javascript.options.baselinejit" = false;
  # "javascript.options.jit_trustedprincipals" = true;
  # "javascript.options.wasm" = false;
  # "gfx.font_rendering.opentype_svg.enabled" = false;
  # "media.eme.enabled" = false;
  # "browser.eme.ui.enabled" = false;
  # "network.dns.disableIPv6" = true;
}
