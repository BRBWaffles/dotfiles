{
  name = "Academic";
  toolbar = false;
  bookmarks = [
    {
      name = "PubMed";
      url = "https://pubmed.ncbi.nlm.nih.gov";
      tags = [
        "pub"
        "pubmed"
        "science"
        "research"
        "academic"
      ];
      keyword = "Pub";
    }
    {
      name = "Sci-Hub";
      url = "https://sci-hub.ee";
      tags = [
        "sci"
        "sci-hub"
        "scihub"
        "science"
        "research"
        "academic"
        "torrent"
      ];
      keyword = "Sci";
    }
    {
      name = "Stanford Encyclopedia of Philosophy";
      url = "https://plato.stanford.edu";
      tags = [
        "standford"
        "encyclopedia"
        "philosophy"
        "phil"
      ];
      keyword = "Phil";
    }
    {
      name = "Zotero Bibliography";
      url = "https://zbib.org";
      tags = [
        "votero"
        "bibliography"
        "bib"
        "zbib"
      ];
      keyword = "Bib";
    }
  ];
}
