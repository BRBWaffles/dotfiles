{
  name = "Tracking";
  toolbar = false;
  bookmarks = [
    {
      name = "1337";
      url = "https://1337x.to";
      tags = [
        "torrent"
        "1337"
      ];
      keyword = "1337";
    }
    {
      name = "Core Radio";
      url = "https://coreradio.online";
      tags = [
        "core"
        "radio"
        "metal"
      ];
      keyword = "Core";
    }
    {
      name = "Metal Tracker";
      url = "https://en.metal-tracker.com";
      tags = [
        "metaltracker"
        "metal"
        "tracker"
      ];
      keyword = "Metal";
    }
    {
      name = "Torrent Leech";
      url = "https://www.torrentleech.org";
      tags = [
        "torrent"
        "leech"
      ];
      keyword = "Leech";
    }
  ];
}
