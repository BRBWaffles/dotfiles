{
  name = "Gaming";
  toolbar = false;
  bookmarks = [
    {
      name = "Battle.net";
      url = "https://us.shop.battle.net";
      tags = [
        "battle.net"
        "battle"
        "net"
      ];
      keyword = "Battle";
    }
    {
      name = "Chess.com";
      url = "https://www.chess.com/home";
      tags = [
        "chess"
      ];
      keyword = "Chess";
    }
    {
      name = "Lichess";
      url = "https://lichess.org";
      tags = [
        "lichess"
        "chess"
      ];
      keyword = "Li";
    }
    {
      name = "ProtonDB";
      url = "https://www.protondb.com";
      tags = [
        "steam"
        "db"
      ];
      keyword = "DB";
    }
    {
      name = "SteamDB";
      url = "https://steamdb.info";
      tags = [
        "steamdb"
        "steam"
        "db"
      ];
      keyword = "SteamDB";
    }
  ];
}
