{
  "Spankbang" = {
    definedAliases = [
      "@sb"
    ];
    icon = ./icons/sb.png;
    urls = [
      { template = "https://spankbang.com/s/{searchTerms}/"; }
    ];
  };
  "XHampster" = {
    definedAliases = [
      "@xh"
    ];
    icon = ./icons/xh.png;
    urls = [
      { template = "https://xhamster.com/search/{searchTerms}"; }
    ];
  };
  "XVideos" = {
    definedAliases = [
      "@xv"
    ];
    icon = ./icons/xv.png;
    urls = [
      { template = "https://www.xvideos.com/?k={searchTerms}"; }
    ];
  };
}
