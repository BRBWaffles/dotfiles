{
  "Reddit" = {
    definedAliases = [
      "@re"
    ];
    icon = ./icons/re.png;
    urls = [
      { template = "https://www.reddit.com/search/?q={searchTerms}"; }
    ];
  };
  "YouTube" = {
    definedAliases = [
      "@yo"
    ];
    icon = ./icons/yo.png;
    urls = [
      { template = "https://www.youtube.com/results?search_query={searchTerms}"; }
    ];
  };
  "X" = {
    definedAliases = [
      "@tw"
    ];
    icon = ./icons/tw.png;
    urls = [
      { template = "https://x.com/search?q={searchTerms}"; }
    ];
  };
}
