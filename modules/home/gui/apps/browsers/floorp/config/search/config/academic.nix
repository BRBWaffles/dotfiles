{
  "Sci-Hub" = {
    definedAliases = [
      "@sc"
    ];
    icon = ./icons/sc.png;
    urls = [
      { template = "https://sci-hub.ee/{searchTerms}"; }
    ];
  };
  "Wikipedia" = {
    definedAliases = [
      "@wi"
    ];
    urls = [
      { template = "https://en.wikipedia.org/wiki/{searchTerms}"; }
    ];
  };
}
