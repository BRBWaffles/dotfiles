{
  "ui.systemUsesDarkTheme" = 1;
  "browser.aboutConfig.showWarning" = false;
  "browser.aboutwelcome.enabled" = false;
  "browser.bookmarks.addedImportButton" = false;
  "browser.toolbars.bookmarks.visibility" = "never";
  "browser.urlbar.suggest.history" = false;
  "browser.urlbar.suggest.bookmark" = true;
  "browser.urlbar.suggest.openpage" = false;
  "browser.urlbar.suggest.shortcut" = false;
  "browser.urlbar.suggest.searches" = false;
  "browser.urlbar.suggest.recentsearches" = false;
  "dom.forms.autocomplete.formautofill" = true;
  "extensions.pocket.enabled" = false;
  "general.autoScroll" = true;
  "media.eme.enabled" = true;
  "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
  "browser.compactmode.show" = true;
  "browser.display.use_document_fonts" = true;
  "gfx.downloadable_fonts.enabled" = true;
}
