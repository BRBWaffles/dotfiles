{
  # BLOCK IMPLICIT OUTBOUND
  "network.prefetch-next" = false;
  "network.dns.disablePrefetch" = true;
  "network.predictor.enabled" = false;
  "network.predictor.enable-prefetch" = false;
  "network.http.speculative-parallel-limit" = 0;
  "browser.places.speculativeConnect.enabled" = false;
}
