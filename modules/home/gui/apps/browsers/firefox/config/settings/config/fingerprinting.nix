{
  # FPP (fingerprintingProtection)
  "privacy.fingerprintingProtection.pbmode" = true;
  "privacy.fingerprintingProtection" = true;

  # RFP (resistFingerprinting)
  "privacy.resistFingerprinting" = false;
  "privacy.window.maxInnerWidth" = 1600;
  "privacy.window.maxInnerHeight" = 900;
  "privacy.resistFingerprinting.block_mozAddonManager" = true;
  "privacy.resistFingerprinting.letterboxing" = false;
  "privacy.spoof_english" = 1;
  "browser.display.use_system_colors" = false;
  "widget.non-native-theme.enabled" = true;
  "browser.link.open_newwindow" = 3;
  "browser.link.open_newwindow.restriction" = 0;
  "webgl.disabled" = true;
}
