{
  # CONTAINERS
  "privacy.userContext.enabled" = true;
  "privacy.userContext.ui.enabled" = true;
  # DOM (DOCUMENT OBJECT MODEL)
  "dom.disable_window_move_resize" = true;
  # MISCELLANEOUS
  "browser.safebrowsing.downloads.remote.enabled" = false;
  "browser.download.start_downloads_in_tmp_dir" = true;
  "browser.helperApps.deleteTempFileOnExit" = true;
  "browser.uitour.enabled" = false;
  "devtools.debugger.remote-enabled" = false;
  "network.IDN_show_punycode" = true;
  "pdfjs.disabled" = false;
  "pdfjs.enableScripting" = false;
  # PLUGINS / MEDIA / WEBRTC
  "media.peerconnection.ice.proxy_only_if_behind_proxy" = true;
  "media.peerconnection.ice.default_address_only" = true;
  # REFERERS
  "network.http.referer.XOriginTrimmingPolicy" = 2;
  # NON-PROJECT RELATED
  # "browser.startup.homepage_override.mstone" = "ignore";
  "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons" = false;
  "browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features" = false;
  "browser.urlbar.showSearchTerms.enabled" = false;
}
