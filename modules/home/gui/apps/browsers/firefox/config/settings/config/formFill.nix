{
  # LOCATION BAR / SEARCH BAR / SUGGESTIONS / HISTORY / FORMS
  "browser.formfill.enable" = false;
  "browser.search.separatePrivateDefault.ui.enabled" = true;
  "browser.search.separatePrivateDefault" = true;
  "browser.search.suggest.enabled" = false;
  "browser.urlbar.maxRichResults" = 16;
  "browser.urlbar.speculativeConnect.enabled" = false;
  "extensions.formautofill.addresses.enabled" = false;
  "extensions.formautofill.addresses.supported" = "on";
  "extensions.formautofill.addresses.usage.hasEntry" = true;
  "extensions.formautofill.creditCards.enabled" = false;
  "extensions.formautofill.heuristics.enabled" = false;
}
