{
  # DON'T TOUCH
  "extensions.blocklist.enabled" = true;
  "network.http.referer.spoofSource" = false;
  "security.dialog_enable_delay" = 1000;
  "privacy.firstparty.isolate" = false;
  "extensions.webcompat.enable_shims" = true;
  "security.tls.version.enable-deprecated" = false;
  "extensions.webcompat-reporter.enabled" = false;
  "extensions.quarantinedDomains.enabled" = true;
}
