{
  "browser.newtab.url" = "about:home";
  "browser.newtabpage.activity-stream.default.sites" = "";
  "browser.newtabpage.activity-stream.feeds.system.topstories" = false;
  "browser.newtabpage.activity-stream.feeds.topsites" = false;
  "browser.newtabpage.activity-stream.showRecentSaves" = false;
  "browser.newtabpage.activity-stream.showSearch" = false;
  "browser.newtabpage.activity-stream.showSponsored" = false;
  "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
  "browser.newtabpage.activity-stream.showWeather" = false;
  "browser.newtabpage.enabled" = true;
  "browser.startup.homepage" = "about:home";
  "browser.startup.page" = 1;
  "browser.tabs.closeWindowWithLastTab" = false;
  "browser.tabs.firefox-view-newIcon" = false;
  "browser.tabs.firefox-view" = false;
  "browser.tabs.inTitlebar" = 1;
  "browser.tabs.loadBookmarksInBackground" = true;
  "browser.tabs.tabmanager.enabled" = false;
}
