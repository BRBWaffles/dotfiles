{
  # SHUTDOWN & SANITIZING
  "privacy.sanitize.sanitizeOnShutdown" = false;
  "privacy.clearOnShutdown.cache" = true;
  "privacy.clearOnShutdown.downloads" = true;
  "privacy.clearOnShutdown.formdata" = true;
  "privacy.clearOnShutdown.history" = true;
  "privacy.clearOnShutdown.cookies" = true;
  "privacy.clearOnShutdown.offlineApps" = true;
  "privacy.clearOnShutdown.sessions" = false;
  "privacy.clearSiteData.cache" = true;
  "privacy.clearSiteData.historyFormDataAndDownloads" = true;
  "privacy.clearHistory.cache" = true;
  "privacy.clearHistory.cookiesAndStorage" = false;
  "privacy.clearHistory.historyFormDataAndDownloads" = true;
  "privacy.cpd.sessions" = true;
  # SHUTDOWN & SANITIZING (continued)
  "privacy.clearOnShutdown_v2.cache" = true;
  "privacy.clearOnShutdown_v2.historyFormDataAndDownloads" = true;
  "privacy.clearOnShutdown_v2.cookiesAndStorage" = true;
}
