{
  # ETP (ENHANCED TRACKING PROTECTION)
  "browser.contentblocking.category" = "strict";
  # OPTIONAL OPSEC
  "browser.download.useDownloadDir" = false;
  "browser.download.alwaysOpenPanel" = false;
  "browser.download.manager.addToRecentDocs" = false;
  "browser.download.always_ask_before_handling_new_types" = true;
  "extensions.enabledScopes" = 5;
  "extensions.postDownloadThirdPartyPrompt" = false;
}
