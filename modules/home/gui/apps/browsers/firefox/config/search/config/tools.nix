{
  "AlternativeTo" = {
    definedAliases = [
      "@al"
    ];
    icon = ./icons/al.png;
    urls = [
      { template = "https://alternativeto.net/browse/search/?q={searchTerms}"; }
    ];
  };
  "Rhyme Zone" = {
    definedAliases = [
      "@rz"
    ];
    icon = ./icons/rz.png;
    urls = [
      {
        template = "https://www.rhymezone.com/r/rhyme.cgi?Word={searchTerms}&typeofrhyme=perfect&org1=syl&org2=l&org3=y";
      }
    ];
  };
  "Urban Dictionary" = {
    definedAliases = [
      "@ur"
    ];
    icon = ./icons/ur.png;
    urls = [
      { template = "https://www.urbandictionary.com/define.php?term={searchTerms}"; }
    ];
  };
  "Stanford Encyclopedia of Philosophy" = {
    definedAliases = [
      "@ph"
    ];
    icon = ./icons/ph.png;
    urls = [
      { template = "https://plato.stanford.edu/search/searcher.py?query={searchTerms}"; }
    ];
  };
}
