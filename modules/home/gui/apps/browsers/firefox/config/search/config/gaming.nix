{
  "Proton DB" = {
    definedAliases = [
      "@pd"
    ];
    icon = ./icons/pd.png;
    urls = [
      { template = "https://www.protondb.com/search?q={searchTerms}"; }
    ];
  };

  "Steam DB" = {
    definedAliases = [
      "@sd"
    ];
    icon = ./icons/sd.png;
    urls = [
      { template = "https://steamdb.info/search/?a=all&q={searchTerms}"; }
    ];
  };
}
