{
  "Brave" = {
    definedAliases = [
      "@br"
    ];
    icon = ./icons/br.png;
    urls = [
      { template = "https://search.brave.com/search?q={searchTerms}&source=web"; }
    ];
  };
  "Kagi" = {
    definedAliases = [
      "@ka"
    ];
    icon = ./icons/ka.png;
    urls = [
      { template = "https://kagi.com/search?q={searchTerms}"; }
    ];
  };
}
