{
  name = "Finances";
  toolbar = false;
  bookmarks = [
    {
      name = "Adsense";
      url = "https://www.google.com/adsense/new/u/0/pub-4524791551954022/payments";
      tags = [
        "google"
        "adsense"
        "ads"
        "money"
      ];
      keyword = "Adsense";
    }
    {
      name = "Canada Revenue Agency";
      url = "https://apps4.ams-sga.cra-arc.gc.ca/gol-ged/awsc/amss/browser/check?program=mima&target=login&lang=en&idp=cms";
      tags = [
        "cra"
        "canada"
        "money"
      ];
      keyword = "CRA";
    }
    {
      name = "Credit Karma";
      url = "https://www.creditkarma.ca";
      tags = [
        "credit"
        "karma"
        "bank"
        "banking"
      ];
      keyword = "Credit";
    }
    {
      name = "Kijiji";
      url = "https://www.kijiji.ca";
      tags = [
        "kijiji"
        "kij"
        "ki"
      ];
      keyword = "Kij";
    }
    {
      name = "LiberaPay";
      url = "https://liberapay.com/upRootNutrition";
      tags = [
        "libera"
        "liberapay"
        "donations"
        "bank"
        "banking"
        "money"
        "uprootnutrition"
      ];
      keyword = "Libera";
    }
    {
      name = "Patreon";
      url = "https://www.patreon.com/upRootNutrition";
      tags = [
        "patreon"
        "donations"
        "bank"
        "banking"
        "money"
        "uprootnutrition"
      ];
      keyword = "Patreon";
    }
    {
      name = "PayPal";
      url = "https://www.paypal.com/myaccount/summary?intl=0";
      tags = [
        "paypal"
        "bank"
        "banking"
        "money"
      ];
      keyword = "PayPal";
    }
    {
      name = "Shopify";
      url = "https://uprootnutrition.myshopify.com/admin";
      tags = [
        "shopify"
        "business"
        "shop"
        "bank"
        "banking"
        "money"
      ];
      keyword = "Shopify";
    }
    {
      name = "Simplii";
      url = "https://online.simplii.com/ebm-resources/public/client/web/index.html#/signon";
      tags = [
        "simplii"
        "bank"
        "banking"
        "money"
      ];
      keyword = "Simplii";
    }
    {
      name = "Stripe";
      url = "https://dashboard.stripe.com/settings/user";
      tags = [
        "stripe"
        "bank"
        "banking"
        "money"
      ];
      keyword = "Stripe";
    }
    {
      name = "Telus (Security)";
      url = "https://smarthome-security.telus.com/web/system/home";
      tags = [
        "telus"
        "security"
      ];
      keyword = "ADT";
    }
    {
      name = "Wealthsimple";
      url = "https://my.wealthsimple.com/app/tax-onboarding/2021";
      tags = [
        "wealth"
        "simple"
        "bank"
        "banking"
        "crypto"
      ];
      keyword = "Wealth";
    }
    {
      name = "Wix";
      url = "https://manage.wix.com/dashboard/413fd74d-8a8f-4c77-bd91-9ea560ffe906/home";
      tags = [
        "wix"
        "website"
        "business"
        "bills"
        "uprootnutrition"
      ];
      keyword = "Wix";
    }
  ];
}
