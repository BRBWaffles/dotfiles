{
  name = "Shopping";
  toolbar = false;
  bookmarks = [
    {
      name = "Amazon";
      url = "https://www.amazon.ca";
      tags = [
        "amazon"
        "shopping"
        "supply"
      ];
      keyword = "Amazon";
    }
    {
      name = "Door Dash";
      url = "https://www.doordash.com";
      tags = [
        "doordash"
        "door"
        "dash"
        "food"
      ];
      keyword = "Dash";
    }
    {
      name = "Fiverr";
      url = "https://www.fiverr.com";
      tags = [
        "fiverr"
        "graphic"
        "design"
      ];
      keyword = "Five";
    }
    {
      name = "FTY Supplies";
      url = "https://fytsupplies.ca";
      tags = [
        "tattoo tat"
        "fyt"
        "shopping"
        "supply"
      ];
      keyword = "FYT";
    }
    {
      name = "Skip the Dishes";
      url = "https://www.skipthedishes.com";
      tags = [
        "skip"
        "dishes"
        "food"
      ];
      keyword = "Skip";
    }
    {
      name = "Uber Eats";
      url = "https://www.ubereats.com";
      tags = [
        "uber"
        "eats"
        "food"
      ];
      keyword = "Uber";
    }
    {
      name = "TatSoul";
      url = "https://www.tatsoul.com";
      tags = [
        "tattoo"
        "tat"
        "tatsoul"
        "shopping"
        "supply"
      ];
      keyword = "TatSoul";
    }
  ];
}
