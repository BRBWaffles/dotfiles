{
  name = "Tools";
  toolbar = false;
  bookmarks = [
    {
      name = "Mullvad";
      url = "https://mullvad.net";
      tags = [
        "mullvad"
        "vpn"
      ];
      keyword = "Mull";
    }
    {
      name = "Cyanophage Layout Analyzer";
      url = "https://cyanophage.github.io";
      tags = [
        "cyanophage"
        "keyboard"
        "layout"
      ];
      keyword = "Layout";
    }
    {
      name = "Smtp2Go";
      url = "https://app-us.smtp2go.com/dashboard/main";
      tags = [
        "smtp"
        "email"
      ];
      keyword = "Smtp";
    }
    {
      name = "Oryx";
      url = "https://configure.zsa.io/moonlander";
      tags = [
        "zsa"
        "moonlander"
        "keyboard"
        "layout"
      ];
      keyword = "Moon";
    }
    {
      name = "Hugging Face";
      url = "https://www.huggingface.co";
      tags = [
        "hugging"
        "face"
        "ai"
        "gguf"
      ];
      keyword = "Hug";
    }
    {
      name = "DeepSeek";
      url = "https://chat.deepseek.com/";
      tags = [
        "deepseek"
        "deep"
        "seek"
        "ai"
      ];
      keyword = "Deep";
    }
    {
      name = "Uncensored AI";
      url = "https://www.aiuncensored.info";
      tags = [
        "uncensored"
        "ai"
      ];
      keyword = "AI";
    }
    {
      name = "Canadian Blood Services";
      url = "https://www.blood.ca/en";
      tags = [
        "canadian"
        "blood"
        "services"
        "give"
      ];
      keyword = "Give";
    }
    {
      name = "Chmod Calculator";
      url = "https://chmod-calculator.com";
      tags = [
        "chmod"
        "calculator"
      ];
      keyword = "Chmod";
    }
    {
      name = "ChatGPT";
      url = "https://chatgpt.com";
      tags = [
        "chat"
        "chatgpt"
        "gpt"
      ];
      keyword = "Chat";
    }
    {
      name = "Claude AI";
      url = "https://claude.ai";
      tags = [
        "claude"
        "ai"
      ];
      keyword = "Claude";
    }
    {
      name = "Cronometer";
      url = "https://cronometer.com/#diary";
      tags = [
        "cronometer"
        "cron"
        "nutrition"
      ];
      keyword = "Cron";
    }
    {
      name = "DNS Checker";
      url = "https://dnschecker.org";
      tags = [
        "dns"
        "checker"
      ];
      keyword = "DNS";
    }
    {
      name = "EventBrite";
      url = "https://www.eventbrite.ca/d/canada--winnipeg/events--today/winnipeg/?page=1";
      tags = [
        "eventbrite"
        "event"
        "brite"
      ];
      keyword = "Event";
    }
    {
      name = "Google Maps";
      url = "https://www.google.com/maps";
      tags = [
        "maps"
        "google"
      ];
      keyword = "Map";
    }
    {
      name = "ListenBrainz";
      url = "https://listenbrainz.org/user/BRBWaffles";
      tags = [
        "listenbrainz"
        "listen"
        "brains"
      ];
      keyword = "Listen";
    }
    {
      name = "Memory Express";
      url = "https://www.memoryexpress.com";
      tags = [
        "memoryexpress"
        "memory"
        "mem"
        "express"
      ];
      keyword = "Mem";
    }
    {
      name = "Percentage Calculator";
      url = "https://percentagecalculator.net";
      tags = [
        "percentage"
        "percent"
        "calculator"
      ];
      keyword = "Percent";
    }
    {
      name = "Portchecker";
      url = "https://portchecker.co";
      tags = [
        "portchecker"
        "port"
        "checker"
      ];
      keyword = "Port";
    }
    {
      name = "Proof Tree";
      url = "https://www.umsu.de/trees";
      tags = [
        "proof"
        "tree"
        "logic"
        "academic"
      ];
      keyword = "Logic";
    }
    {
      name = "RhymeZone";
      url = "https://www.rhymezone.com";
      tags = [
        "rhymezone"
        "rhyme"
        "zone"
      ];
      keyword = "Rhyme";
    }
    {
      name = "Sci-Hub";
      url = "https://sci-hub.ee";
      tags = [
        "sci"
        "sci-hub"
        "scihub"
        "science"
        "research"
        "academic"
        "torrent"
      ];
      keyword = "Sci";
    }
    {
      name = "Speedtest";
      url = "https://www.speedtest.net";
      tags = [
        "speedtest"
        "speed"
        "test"
      ];
      keyword = "Speed";
    }
    {
      name = "Stanford Encyclopedia of Philosophy";
      url = "https://plato.stanford.edu";
      tags = [
        "standford"
        "encyclopedia"
        "philosophy"
        "phil"
      ];
      keyword = "Phil";
    }
    {
      name = "Telus (Billing)";
      url = "https://www.telus.com/my-telus/billing/summary";
      tags = [
        "telus"
        "phone"
        "bills"
        "money"
      ];
      keyword = "Telus";
    }
    {
      name = "TinEye";
      url = "https://tineye.com";
      tags = [
        "tineye"
        "tin"
        "eye"
      ];
      keyword = "Tin";
    }
    {
      name = "Zotero Bibliography";
      url = "https://zbib.org";
      tags = [
        "votero"
        "bibliography"
        "bib"
        "zbib"
      ];
      keyword = "Bib";
    }
  ];
}
