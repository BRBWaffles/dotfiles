{
  pre-commit.settings.hooks = {
    nixfmt-rfc-style.enable = true;
    commitizen.enable = true;
    # statix.enable = true;
  };
}
