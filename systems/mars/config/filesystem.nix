{
  flake,
  config,
  ...
}:
let
  inherit (flake.config.people)
    user0
    user1
    ;
  inherit (flake.config.machines.devices)
    mars
    ceres
    synology
    ;
  inherit (flake.config.services.instances)
    samba
    ;

  synologySecrets = config.sops.secrets."network/synology".path;
  ceresSecrets = config.sops.secrets."network/server".path;
in
{
  fileSystems =
    let
      synologyDrives = [
        "folder0"
        "folder1"
        "folder2"
      ];

      storageDrives = [
        "storage0"
        "storage1"
      ];

      sambaDrives = [
        "samba0"
      ];

      synologyMounts = synologyDrive: {
        name = "${synology.${synologyDrive}.mount}";
        value = {
          device = synology.${synologyDrive}.device;
          fsType = "cifs";
          options = synology.${synologyDrive}.options ++ [
            "credentials=${synologySecrets}"
          ];
        };
      };

      storageMounts = storageDrive: {
        name = "${mars.${storageDrive}.mount}";
        value = {
          device = mars.${storageDrive}.device;
          fsType = "ext4";
          options = mars.${storageDrive}.options;
        };
      };

      sambaMounts = sambaDrive: {
        name = "${ceres.${sambaDrive}.mount}/${samba.paths.path1}";
        value = {
          device = "${ceres.${sambaDrive}.device}/${samba.paths.path1}";
          fsType = "cifs";
          options = ceres.${sambaDrive}.options ++ [
            "credentials=${ceresSecrets}"
          ];
        };
      };
    in
    {
      "/" = {
        device = "/dev/disk/by-uuid/57e88a48-00eb-46ca-9e2a-855de7e066b2";
        fsType = "ext4";
      };
      "/boot" = {
        device = "/dev/disk/by-uuid/F095-17B7";
        fsType = "vfat";
        options = mars.boot.options;
      };
    }
    // (builtins.listToAttrs (map synologyMounts synologyDrives))
    // (builtins.listToAttrs (map storageMounts storageDrives))
    // (builtins.listToAttrs (map sambaMounts sambaDrives));

  swapDevices = [
    { device = "/dev/disk/by-uuid/56c84b5e-83b4-4ea1-b6ec-a29fc057292d"; }
  ];

  systemd.tmpfiles.rules = [
    "Z ${config.home-manager.users.${user0}.home.homeDirectory} 0755 ${user0} users -"
    "Z ${config.home-manager.users.${user1}.home.homeDirectory} 0755 ${user1} users -"
    "Z ${mars.storage0.mount} 0755 ${user0} users -"
    "Z ${mars.storage1.mount} 0755 ${user0} users -"
  ];

  services.udisks2.enable = true;
}
