{
  flake,
  config,
  ...
}:
let
  inherit (flake.config.people)
    user0
    user1
    ;
  inherit (flake.config.machines.devices)
    deimos
    synology
    ceres
    ;
  inherit (flake.config.services.instances)
    samba
    ;

  synologySecrets = config.sops.secrets."network/synology".path;
  ceresSecrets = config.sops.secrets."network/server".path;
in
{
  fileSystems =
    let
      synologyDrives = [
        "folder0"
        "folder1"
        "folder2"
      ];

      sambaDrives = [
        "samba0"
      ];

      synologyMounts = synologyDrive: {
        name = "${synology.${synologyDrive}.mount}";
        value = {
          device = synology.${synologyDrive}.device;
          fsType = "cifs";
          options = synology.${synologyDrive}.options ++ [
            "credentials=${synologySecrets}"
          ];
        };
      };

      sambaMounts = sambaDrive: {
        name = "${ceres.${sambaDrive}.mount}/${samba.paths.path1}";
        value = {
          device = "${ceres.${sambaDrive}.device}/${samba.paths.path1}";
          fsType = "cifs";
          options = ceres.${sambaDrive}.options ++ [
            "credentials=${ceresSecrets}"
          ];
        };
      };
    in
    {
      "/" = {
        device = "/dev/disk/by-uuid/aae33e00-125e-4f1c-b9aa-9fc512335b7f";
        fsType = "ext4";
      };
      "/boot" = {
        device = "/dev/disk/by-uuid/C506-9E18";
        fsType = "vfat";
        options = deimos.boot.options;
      };
    }
    // (builtins.listToAttrs (map synologyMounts synologyDrives))
    // (builtins.listToAttrs (map sambaMounts sambaDrives));

  swapDevices = [
    { device = "/dev/disk/by-uuid/68d6072a-e21b-4d11-88fd-22c5b9932a50"; }
  ];

  systemd.tmpfiles.rules = [
    "Z ${config.home-manager.users.${user0}.home.homeDirectory} 0755 ${user0} users -"
    "Z ${config.home-manager.users.${user1}.home.homeDirectory} 0755 ${user1} users -"
  ];

  services.udisks2.enable = true;
}
